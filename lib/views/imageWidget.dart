import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:project/views/new_item.dart';

//Contiene la fuente de la imagen. Camara o galeria
enum ImageSourceType { gallery, camera }

class ImageFromGalleryEx extends StatefulWidget {
  final type;
  ImageFromGalleryEx(this.type);

  @override
  ImageFromGalleryExState createState() => ImageFromGalleryExState(this.type);
}

//Widget that holds the state of the ImageFromGallery
class ImageFromGalleryExState extends State<ImageFromGalleryEx> {
  var _image; //Holds picked image
  var imagePicker; // Holds instance of imagepicker
  var type; //Holds type of image

  ImageFromGalleryExState(this.type);

  //Method used too initialize and create the instance of the imagePicker
  @override
  void initState() {
    ImagePicker picker = ImagePicker(); //

    super.initState();
    imagePicker = new ImagePicker();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(type == ImageSourceType.camera ? "Camera" : "Gallery")),
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 52,
          ),
          Center(
            child: GestureDetector(
              onTap: () async {
                var source = type == ImageSourceType.camera
                    ? ImageSource.camera
                    : ImageSource.gallery;
                XFile image = await imagePicker.pickImage(
                    source: source,
                    imageQuality: 50,
                    preferredCameraDevice: CameraDevice.front);

                //Asignamos la imagen como un estado
                setState(() {
                  _image = File(image.path);
                });
              },
              child: Container(
                width: 200,
                height: 200,
                decoration: BoxDecoration(color:Color.fromARGB(255, 218, 146, 82)),
                child: _image != null
                    //If there is an image, render it
                    ? Image.file(
                        //Renderiza la imagen
                        _image,
                        width: 200.0,
                        height: 200.0,
                        fit: BoxFit.fitHeight,
                      )
                    //If there is not an image, display camera icon

                    : Container(
                        decoration: BoxDecoration(color: Color.fromARGB(255, 218, 146, 82)),
                        width: 200,
                        height: 200,
                        child: Icon(
                          Icons.camera_alt,
                          color: Colors.grey[800],
                        ),
                      ),
              ),
            ),
          ),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Color.fromARGB(255, 218, 146, 82),
                  minimumSize: const Size(100, 25),
                  maximumSize: const Size(100, 25)),
              child: Text('Done'),
              onPressed: () {
                Navigator.pop(context, _image);
              }),

          //MaterialPageRoute(builder: (context) => MyHome())
        ],
      ),
    );
  }
}
