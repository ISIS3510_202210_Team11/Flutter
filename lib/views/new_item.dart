import 'package:flutter/material.dart';
import 'package:project/views/imageWidget.dart';
import 'dart:io';

import 'package:lite_rolling_switch/lite_rolling_switch.dart';
import 'package:firebase_performance/firebase_performance.dart';

FirebasePerformance performance = FirebasePerformance.instance;

void main() async {
  //runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: MyHome());
  }
}

class MyHome extends StatefulWidget {
  @override
  _MyHomeState createState() => _MyHomeState();
}

class _MyHomeState extends State<MyHome> {

  Trace trace = performance.newTrace('newItem');

  @override
  void initState() {
    // TODO: implement initState
    trace.start();
  }

  var path_imagen;
  //Function which opens the image widget. Espera a que le devuelvan la imagen
  void _handleURLButtonPress(BuildContext context, var type) async {
    final File imagen = await Navigator.push(context,
        MaterialPageRoute(builder: (context) => ImageFromGalleryEx(type)));

    //Asigna la imagen que el usuario escogió
    setState(() {
      path_imagen = File(imagen.path);
    });
  }

  @override
  Widget build(BuildContext context) {
    trace.stop();
    return Scaffold(
      backgroundColor: Color(0xffE5E5E5),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'New Item',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40),
              ),

              //Container to show image
              Container(
                height: 300,
                width: 300,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  image: DecorationImage(
                    image: NetworkImage(
                        "https://media.istockphoto.com/photos/flowery-evase-bateau-yellow-dress-picture-id178851955?k=20&m=178851955&s=612x612&w=0&h=PbB6SOjyZNIzTYr3qC7IY-qn1tsWWHyojRfs4XB8WhM="),
                  ),
                ),
              ),

              //Fila de Botones
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Color.fromARGB(255, 218, 146, 82),
                        minimumSize: const Size(100, 25),
                        maximumSize: const Size(100, 25)),
                    child: Text('Gallery'),
                    onPressed: () {
                      //print("Gallery");
                      _handleURLButtonPress(context, ImageSourceType.gallery);
                    }),
                SizedBox(width: 50),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Color.fromARGB(255, 218, 146, 82),
                        minimumSize: const Size(100, 25),
                        maximumSize: const Size(100, 25)),
                    child: Text('Camera'),
                    onPressed: () {
                      _handleURLButtonPress(context, ImageSourceType.camera);
                    })
              ]),
              SizedBox(
                height: 20,
                width: 200,
                child: Divider(color: Colors.white),
              ),

              //Card for the Form
              Card(
                color: Colors.white,
                margin: EdgeInsets.symmetric(vertical: 10, horizontal: 65),
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(20.0, 0, 20, 5),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: 'Product Name',
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20.0, 0, 20, 5),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: 'Price',
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20.0, 0, 20, 5),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: 'New or used',
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20.0, 0, 20, 5),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: 'Price',
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20.0, 0, 20, 5),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: 'Tags',
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              // Buttons Swappable and done
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                SizedBox(
                  width: 125.0,
                  height: 27.0,
                  child: LiteRollingSwitch(
                    value: false,
                    textOn: "Swappable",
                    textOff: "Swappable",
                    colorOn: Colors.greenAccent,
                    colorOff: Colors.redAccent,
                    iconOn: Icons.done,
                    iconOff: Icons.close,
                    textSize: 15,
                    onChanged: (bool position) {
                      print(" Swappable is $position");
                    },
                  ),
                ),
                SizedBox(width: 50),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Color.fromARGB(255, 218, 146, 82),
                        minimumSize: const Size(100, 25),
                        maximumSize: const Size(100, 25)),
                    child: Text('Done'),
                    onPressed: () {
                      print('Your item has been added');
                    })
              ]),
            ],
          ),
        ),
      ),
    );
  }
}
