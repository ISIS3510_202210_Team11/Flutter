import 'package:flutter/cupertino.dart';
import 'package:project/controllers/firebaseController.dart';
import 'package:project/models/userInfo.dart';
import 'package:flutter/material.dart';
import 'package:project/models/user.dart';
import 'package:project/views/profile/profileWidget.dart';
import 'package:project/views/profile/buttonWidget.dart';
import 'package:project/views/profile/appBarWidget.dart';
import 'dart:io';
import 'package:project/views/new_item.dart';
import 'package:firebase_performance/firebase_performance.dart';

FirebasePerformance performance = FirebasePerformance.instance;

class UserPage extends StatefulWidget {
const UserPage ({
Key? key, 
required this.user,

}) :super(key: key); 

final User user;

 @override 
 _ProfilePageState  createState() => _ProfilePageState();


}

 class _ProfilePageState extends State<UserPage>
 {
   final Trace trace = performance.newTrace('UserPage');

   @override
   void initState() {
     // TODO: implement initState
     trace.start();
   }

   @override
   Widget build(BuildContext context)
   {
     trace.stop();
     //final user =UserInfo.myUser;
     //String key = "KXitKhuh6uhMbB3XfVgB0FZpy8w1";
     //dynamic userdata =  FirebaseController.getUser(key);
     
     //final User user = User.fromJson(userdata,key);

     final ScrollController _controller = ScrollController();

        return Scaffold(
        appBar: buildAppBar(context),

        body:ListView(controller: _controller,
        
        //physics:BouncingScrollPhysics(),
        children:[

          Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
         Align(alignment:Alignment.centerLeft, child:buildName()),Align(alignment:Alignment.centerRight, child:ProfileWidget(  imgPath: widget.user.imgPath,  onClicked: () async {}, )),
        ]),
          
           
        Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            Center(child:buildInfoButton()),
            Center(child:buildPostsButton()),
        ]),

        const SizedBox(width: 50),
            Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            Center(child:buildPurchasesButton()),
            Center(child:buildSettingsButton()), 
        ]),
           
          const SizedBox(height: 48),
          GridView.count(crossAxisCount: 2, shrinkWrap:true,children: [ buildData(),
          buildStats(),],),
          Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [Text('Purchases',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: Color.fromARGB(255, 49, 68, 93),))]),

          


          Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [Text('',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24))]),
         
          purchases(),
          
          ],
        ),

     );
   }

   Widget buildName() => Column(
        children: [
          Row(children:[
          Text(
            widget.user.name,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24,color: Color.fromARGB(255, 49, 68, 93), ),

            )
          
        ]),
          const SizedBox(height: 4),
          Text(
            widget.user.lastName,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: Color.fromARGB(255, 49, 68, 93),),
          ),
          const SizedBox(height: 4),
          Row(children:[
            buildLocationIcon(),
          Text(
            widget.user.location,
            style: TextStyle(color: Colors.grey),
          ),
          buildPostIcon(),
          ]
          )
        ],
      );


  Widget buildPostIcon() =>  IconButton(icon:Icon(
            Icons.add_box,
            color:  Color.fromARGB(255, 49, 68, 93),
            size: 20,

      ), onPressed:()async { await Navigator.of(context).push(
                           MaterialPageRoute(
                               builder: (contex) => MyHome(),
                          ),
                       );},);

Widget buildLocationIcon() =>  Icon(
            Icons.place,
            color:  Color.fromARGB(255, 49, 68, 93),
            size: 20,

      );

  Widget buildInfoButton() => ButtonWidget(
        text: 'Information',
        onClicked: () {},
      );

      Widget buildPostsButton() => ButtonWidget(
        text: 'Posts',
        onClicked: () {},
      );

       Widget buildPurchasesButton() => ButtonWidget(
        text: 'Purchases',
        onClicked: () {},
      );

       Widget buildSettingsButton() => ButtonWidget(
        text: 'Settings',
        onClicked: () {},
      );

   Widget buildData() => Container(
        padding: EdgeInsets.symmetric(horizontal: 48),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children:
           [
             
            Text(
              'Gender',
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 1),
            Text(
               widget.user.gender,
              style: TextStyle(fontSize: 16, height: 1.4),
            ),
            Text(
              'BirthDate',
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 1),
            Text(
               widget.user.birthDate,
              style: TextStyle(fontSize: 16, height: 1.4),
            ),
             ],
        ),
      ); 

       Widget buildStats() => Container(
         
        padding: EdgeInsets.symmetric(horizontal: 48),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [ 
            Icon(
            Icons.nature,
            color: Colors.green,
            size: 20,
            ),
            Text(
              'Trees planted',
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            
            Text(
               widget.user.treesPlanted.toString(),
              style: TextStyle(fontSize: 16, height: 1.4),
            ),
           Icon(
            Icons.shopping_cart,
            color: Colors.blueAccent,
            size: 20,
            ),

            Text(
              'Saved Textile Waste:',
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            Text(
               widget.user.savedTextWaste.toString(),
              style: TextStyle(fontSize: 16, height: 1.4),
            ),
          ],
        ),
      ); 


      Widget purchases() => SingleChildScrollView(
            child: Container(
              child: Column(
                children: <Widget>[
                  Container(
                    height: 200,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: <Widget>[
                        Container(
                          width: 200,
                          color: Colors.white,
                          child:Image( image: NetworkImage('https://i.pinimg.com/564x/0f/fb/7e/0ffb7eb910f78a9b463086052316310d.jpg')),
                        ),
                        Container(
                          width: 200,
                          color: Colors.white,
                          child:Image( image: NetworkImage('https://i.pinimg.com/564x/9a/5c/c1/9a5cc19b3f79636aae0a5420fb129c2b.jpg')),
                        ),
                        Container(
                          width: 200,
                          color: Colors.white,
                          child:Image( image: NetworkImage('https://i.pinimg.com/564x/cf/ea/83/cfea839b49b0bc036ce061e213dbb182.jpg')),
                        ),
                        Container(
                          width: 200,
                          color: Colors.white,
                          child:Image( image: NetworkImage('https://i.pinimg.com/564x/3b/88/4f/3b884f2773ad87f37e9f1721a4603f98.jpg')),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
      );
 }
