import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:project/views/profile/userPage.dart';
import 'package:project/views/home/homepage.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:project/controllers/notificatonController.dart';
import 'package:geolocator/geolocator.dart';
import 'package:firebase_performance/firebase_performance.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  //runApp( login());
}

final _auth = FirebaseAuth.instance;
FirebaseAnalytics analytics = FirebaseAnalytics.instance;
String email = '';
String password = '';

    
    late String currentAddress;
    Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
    FirebasePerformance performance = FirebasePerformance.instance;


getCurrentLocation()async {
    Position position = await geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
    
    List<Placemark> p = await geolocator.placemarkFromCoordinates(
        position.latitude, position.longitude);
    Placemark place = p[0];
    currentAddress= "${place.locality}, ${place.postalCode}, ${place.country}";
    }
class login extends StatefulWidget {
   login({Key? key}) : super(key: key);

  @override
  State<login> createState() => _loginState();
}

class _loginState extends State<login> {
   Trace trace = performance.newTrace('LogIn');

   @override
  void initState() {
    // TODO: implement initState
    trace.start();
  }

  @override
  Widget build(BuildContext context) {
     trace.stop();
    return MaterialApp(
      home: Scaffold(
        body: const MyStatefulWidget(),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _LogIn();
}

class _LogIn extends State<MyStatefulWidget> {

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            Container(
                alignment: Alignment.topLeft,
                padding: const EdgeInsets.all(10),
                child: const Text(
                  'Log in',
                  style: TextStyle(
                      fontSize: 36,
                      fontFamily: 'RobotoMono',
                      fontWeight: FontWeight. bold,
                      color: Color.fromARGB(255, 49, 68, 93),
                  ),
                )),

            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                keyboardType: TextInputType.emailAddress,
                onChanged: (value) {
                  email = value;
                },
                style: const TextStyle(
                  fontSize: 15,
                  fontFamily: 'RobotoMono'
                ),
                decoration: const InputDecoration(
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Color.fromARGB(255, 218, 146, 82),
                        width: 1.8
                    ),
                  ),
                  labelText: 'e-mail@example.com',
                  labelStyle: TextStyle(
                    color: Color.fromARGB(255, 218, 146, 82),
                  ),
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: TextField(
                obscureText: true,
                onChanged: (value) {
                  password = value;
                },
                decoration: const InputDecoration(
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Color.fromARGB(255, 218, 146, 82),
                        width: 1.8
                    ),
                  ),
                  labelText: '··········',
                  labelStyle: TextStyle(
                    color: Color.fromARGB(255, 218, 146, 82),
                    fontFamily: 'RobotoMono'
                  ),
                ),
              ),
            ),

            Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                child: ElevatedButton(
                  style: TextButton.styleFrom(
                    backgroundColor: Color.fromARGB(255, 49, 68, 93), // Background Color
                  ),
                  child: const Text(
                    'LOG IN',
                    style: TextStyle(
                      fontSize: 13,
                      fontFamily: 'RobotoMono',
                        fontWeight: FontWeight. bold
                    ),
                  ),
                  onPressed: ()async {
                    try {
                        await  getCurrentLocation();
                     UserCredential credentials= await _auth.signInWithEmailAndPassword( 
                      email: email,
                      password: password
                      );
                      String id=credentials.user!.uid;
                      await Navigator.of(context).push(
                           MaterialPageRoute(
                               builder: (contex) => HomePage(uid: id, location:currentAddress),
                          ),
                       );
                    } on FirebaseAuthException catch (e) {
                 showDialog(
                    context: context,
                   builder: (ctx) => AlertDialog(
                     title: Text("Ops! Login Failed"),
                   content: Text("The email adress or password must be incorrect")
                     )
                   );
                  };
                  },
                )
            ),

          ],
        ));
  }


}