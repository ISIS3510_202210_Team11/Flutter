import 'package:flutter/material.dart';
import 'package:project/controllers/firebaseController.dart';
import 'package:project/views/profile/buttonWidget.dart';
import 'package:project/views/profile/appBarWidget.dart';
import 'package:project/views/profile/profileWidget.dart';
import 'package:project/views/profile/userPage.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:geolocator/geolocator.dart';
import 'package:project/models/user.dart';
import 'package:firebase_performance/firebase_performance.dart';

FirebasePerformance performance = FirebasePerformance.instance;

class HomePage extends StatefulWidget {

const HomePage ({
Key? key, 
required this.uid,
required this.location,

}) :super(key: key);

final String uid;
final String location;
 @override 
 _ProfilePageState  createState() => _ProfilePageState();
}

 class _ProfilePageState extends State<HomePage>
 {

   final Trace trace = performance.newTrace('HomePage');

   @override
   void initState() {
     // TODO: implement initState
     trace.start();
   }

   /* //Singleton 
    static _ProfilePageState _instance =_ProfilePageState._internal();
    factory _ProfilePageState() 
    {
        return _instance;

    }
   _ProfilePageState._internal(){
     final Position currentPosition= Position();
     final String currentAddress ='';
     final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
     final user =UserInfo.myUser;
   }
     
    */


   //Get location. Taken form https://medium.com/fabcoding/get-current-user-location-in-flutter-57e202bad6db
   
   //final item =UserInfo.myItem
   @override

   Widget build(BuildContext context)
   {
     trace.stop();
     //final product =UserInfo.myUser;
     final ScrollController _controller = ScrollController();
     // final user =UserInfo.myUser;

        return Scaffold(
        appBar: buildAppBar(context),

        body:ListView(controller: _controller,
        
        //physics:BouncingScrollPhysics(),
        children:[

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: 
            [ Column
            (
              children:
              [
         Align(
           alignment:Alignment.centerLeft, child:Text
           ('Hello', style: TextStyle
           (
             fontWeight: FontWeight.bold, fontSize: 24,color: Color.fromARGB(255, 49, 68, 93), 
          
             )
             )
             ),

             Row(children: [Icon(
            Icons.place,
            color: Color.fromARGB(255, 49, 68, 93),
            size: 20
            ),
              Text
           (
             widget.location, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12,color: Color.fromARGB(255, 49, 68, 93)
             )
             )
             ],)
             ]
             ),

             Align(alignment:Alignment.centerRight, child:IconButton(
              icon: Image( image: NetworkImage('https://i.pinimg.com/originals/4a/6d/5b/4a6d5b903a7772b92cb3274b61253e51.jpg')),
              iconSize: 50,
              onPressed: ()async { Map <String, dynamic> userData= await FirebaseController.getUser(widget.uid);
              User user = User(imgPath: userData['photo'], name:userData['name'] , userUID: widget.uid, lastName: userData['surname'], email: userData['email'], location: userData['city'], gender: userData['gender'], birthDate: userData['birthDate'], size: userData['size'], treesPlanted: userData['treesPlanted'], savedTextWaste: userData['savedTextWaste']);

               Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (contex) => UserPage(user: user),
                                ),
                            );},
              )),
              ]),
                
           
         
         Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            Center(child:buildAllButton()),
            Center(child:buildClothingButton()),
            Center(child:buildEntretainmentButton()),
        ]),
          
        GridView.count(crossAxisCount: 2, shrinkWrap:true,children: [ 
          buildData(),buildDataShirt(),buildDataJacket(),buildDataBlazer()   ],),


          ],
        ),

     );
    }

    Widget buildAllButton() => ButtonWidget(
            text: 'All',
            onClicked: () {FirebaseCrashlytics.instance.crash();},
            
          );

      Widget buildClothingButton() => ButtonWidget(
            text: 'Clothing',
            onClicked: () {},
          );    

        Widget buildEntretainmentButton() => ButtonWidget(
                    text: 'Entretainment',
                    onClicked: () {},
                  );

      
Widget buildData() => Container(
        padding: EdgeInsets.symmetric(horizontal: 48),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children:
           [
             Image( image: NetworkImage('https://i.pinimg.com/564x/0f/fb/7e/0ffb7eb910f78a9b463086052316310d.jpg')),
              Text('White woven ruana', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 9,color: Color.fromARGB(255, 49, 68, 93), )),
              Row(children: [Text('50.000'),  Icon(
            Icons.add,
            color: Color.fromARGB(255, 49, 68, 93),
            size: 20
      )
      ],)
             ],
        ),
      ); 

Widget buildDataShirt() => Container(
        padding: EdgeInsets.symmetric(horizontal: 48),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children:
           [
             Image( image: NetworkImage('https://i.pinimg.com/564x/cf/ea/83/cfea839b49b0bc036ce061e213dbb182.jpg')),
              Text('Blue T-Shirt', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 9,color: Color.fromARGB(255, 49, 68, 93), )),
              Row(children: [Text('50.000'),  Icon(
            Icons.add,
            color: Color.fromARGB(255, 49, 68, 93),
            size: 20
      )
      ],)
             ],
        ),
      ); 

      Widget buildDataJacket() => Container(
        padding: EdgeInsets.symmetric(horizontal: 48),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children:
           [
             Image( image: NetworkImage('https://i.pinimg.com/564x/9a/5c/c1/9a5cc19b3f79636aae0a5420fb129c2b.jpg')),
              Text('Terracota Jacket', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 9,color: Color.fromARGB(255, 49, 68, 93), )),
              Row(children: [Text('50.000'),  Icon(
            Icons.add,
            color: Color.fromARGB(255, 49, 68, 93),
            size: 20
      )
      ],)
             ],
        ),
      );

 Widget buildDataBlazer() => Container(
        padding: EdgeInsets.symmetric(horizontal: 48),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children:
           [
              Image( image: NetworkImage('https://i.pinimg.com/564x/3b/88/4f/3b884f2773ad87f37e9f1721a4603f98.jpg')),
              Text('Black Suit', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 9,color: Color.fromARGB(255, 49, 68, 93), )),
              Row(children: [Text('50.000'),  Icon(
            Icons.add,
            color: Color.fromARGB(255, 49, 68, 93),
            size: 20
      )
      ],)
             ],
        ),
      );

Widget buildStats(User user) => Container(
         
        padding: EdgeInsets.symmetric(horizontal: 48),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [ 
            Icon(
            Icons.nature,
            color: Colors.green,
            size: 20,
            ),
            Text(
              'Trees planted',
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            
            Text(
               user.treesPlanted.toString(),
              style: TextStyle(fontSize: 16, height: 1.4),
            ),
           Icon(
            Icons.shopping_cart,
            color: Colors.blueAccent,
            size: 20,
            ),

            Text(
              'Saved Textile Waste:',
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            Text(
               user.savedTextWaste.toString(),
              style: TextStyle(fontSize: 16, height: 1.4),
            ),
          ],
        ),
      ); 





     

  
 }
