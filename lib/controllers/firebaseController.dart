import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:project/models/user.dart';
import 'package:firebase_analytics/firebase_analytics.dart';


class FirebaseController
{
  
  static final FirebaseAuth auth = FirebaseAuth.instance;
  FirebaseAnalytics analytics = FirebaseAnalytics.instance;
 

  static Future <UserCredential> login(String email, String password)async
  {
    UserCredential credential = await auth.signInWithEmailAndPassword(email: email, password: password);
    return credential;
  }

  static  Future<Map<String, dynamic>> getUser(String key)async
    {
      
      DocumentSnapshot documentsnapshot = await FirebaseFirestore.instance.collection('User').doc(key).get();
      print(documentsnapshot.data());
      return documentsnapshot.data()! as Map<String, dynamic>;
   }

   

}