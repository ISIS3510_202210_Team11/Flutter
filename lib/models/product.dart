class Product{

final int id;
final String size;
final String color;
final String name;
final String brand;
final String category;
final bool isSwappable;
final String state;
final String img;
final double price;
final List<String> tags;

const Product ({
required this.id,
required this.size,
required this.color,
required this.name,
required this.brand,
required this.category,
required this.isSwappable,
required this.state,
required this.img,
required this.price,
required this.tags,
});


}