import 'package:flutter/cupertino.dart';

class User {
final String imgPath;
final String name;
final String userUID;
final String lastName;
final String email;
final String location;
final String gender;
final String birthDate;
final String size;
final int treesPlanted;
final int savedTextWaste;

const User ({
required this.imgPath,
required this.name,
required this.userUID,
required this.lastName,
required this.email,
required this.location,
required this.gender,
required this.birthDate,
required this.size,
required this.treesPlanted,
required this.savedTextWaste,
});

static User fromJson(Map <String, dynamic>map, String uid)
{
  return User(

    imgPath: map["photo"],
    name: map["name"],
    userUID: uid,
    lastName: map["surname"],
    email:map["email"],
    location: map["location"],
    gender: map ["gender"],
    birthDate: map["birthDate"],
    size: map["size"],
    treesPlanted: map["treesPlanted"],
    savedTextWaste: map["savedTextWaste"]

  );
}

Map<String, Object?> toJson() {
    return {
      'photo': imgPath,
      'name': name,
      'surname': lastName,
      'email': email,
      'location': location,
      'gender': gender,
      'birthDate': birthDate,
      'size': size,
      'treesPlanted': treesPlanted,
      'savedTextWaste': savedTextWaste,
    };
  }
}